package it.unisalento.springbootapp.conf;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import it.unisalento.springbootapp.actorsimpl.MarlonBrando;
import it.unisalento.springbootapp.actorsimpl.RobertDeNiro;
import it.unisalento.springbootapp.actorsimpl.TomHanks;
import it.unisalento.springbootapp.iactors.IActor;

@Configuration //annotazione per contenere le clasi con bean
public class WebAppConf {
	
	@Bean
	@Qualifier
	public IActor marlonBrando() {
		return new MarlonBrando();
	}

	@Bean
	@Primary
	public IActor robertDeNiro() {
		return new RobertDeNiro();
	}
	
	@Bean
	@Qualifier
	public IActor tomHanks() {
		return new TomHanks();
	}
	
}
