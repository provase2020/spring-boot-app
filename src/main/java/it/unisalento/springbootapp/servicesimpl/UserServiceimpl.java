package it.unisalento.springbootapp.servicesimpl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.unisalento.springbootapp.entities.User;
import it.unisalento.springbootapp.exceptions.UserNotFoundException;
import it.unisalento.springbootapp.iservices.IUserService;
import it.unisalento.springbootapp.repositories.UserRepository;

@Service
public class UserServiceimpl implements IUserService {

	@Autowired
	UserRepository userRepository;
	
	
	@Override
	@Transactional
	public User save(User user) {
		// TODO Auto-generated method stub
		return userRepository.save(user);
	}

	@Override
	@Transactional
	public List<User> getAll() {
		// TODO Auto-generated method stub
		return userRepository.findAll();
	}

	@Override
	@Transactional(rollbackOn = UserNotFoundException.class)
	public User getById(int id) throws UserNotFoundException {
		// TODO Auto-generated method stub
		/*
		User user = userRepository.getOne(id);
		if (user != null) {
			return user;
		}else {
		throw new UserNotFoundException();
		}
		*/
		return userRepository.findById(id).orElseThrow(()-> new UserNotFoundException());
	}

	@Override
	@Transactional(rollbackOn = UserNotFoundException.class)
	public void delete(int id) throws UserNotFoundException {
		// TODO Auto-generated method stub

		User user = userRepository.getOne(id);
		if (user != null) {
			userRepository.delete(user);
		}else {
			throw new UserNotFoundException();
		}
				
	}

	@Override
	public List<User> getByName(String name) {
		// TODO Auto-generated method stub
		return userRepository.findByName(name);
	}

}
