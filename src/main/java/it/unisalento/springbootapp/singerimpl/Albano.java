package it.unisalento.springbootapp.singerimpl;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import it.unisalento.springbootapp.isinger.ISinger;

@Component
@Qualifier
public class Albano implements ISinger {

	@Override
	public void sing() {
		System.out.println("Happiness it's to eat a sandwich!!");
	}
	
	
}
