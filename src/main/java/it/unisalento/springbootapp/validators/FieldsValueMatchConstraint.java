package it.unisalento.springbootapp.validators;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Documented
@Retention(RUNTIME)
@Target(TYPE)
@Constraint(validatedBy = FieldsValueMatchValidator.class )
public @interface FieldsValueMatchConstraint {
	
	String field();
	String fieldMatch();
	
	String message() default "Fields value don't match";
	Class<?>[] groups() default {};
	Class<? extends Payload>[] payload() default {};
	
	@Retention(RUNTIME) 
	@Target(TYPE)
	@interface List {
		
		FieldsValueMatchConstraint[] value();
		
	}

}
