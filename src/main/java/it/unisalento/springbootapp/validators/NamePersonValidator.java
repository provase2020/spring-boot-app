package it.unisalento.springbootapp.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class NamePersonValidator implements ConstraintValidator<NamePersonConstraint, String> {
    
	private String name;
	
	@Override
	public void initialize(NamePersonConstraint constraintAnnotation){
		//TODO Auto_generated method stub
		this.name = constraintAnnotation.name();
	}
	
	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		// TODO Auto-generated method stub
		
		if(this.name.equalsIgnoreCase(value)) {
		
		return false;
		}else{
			return true;
		}
	}

	
	
}
