package it.unisalento.springbootapp.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.BeanWrapperImpl;

public class FieldsValueMatchValidator implements ConstraintValidator<FieldsValueMatchConstraint, Object> {

	String field;
	String fieldMatch;
	
	@Override
	public void initialize(FieldsValueMatchConstraint  constraintAnnotation) {
		// TODO Auto-generated method stub
		this.field = constraintAnnotation.field();
		this.fieldMatch = constraintAnnotation.fieldMatch();
	}
	
	@Override
	public boolean isValid(Object value, ConstraintValidatorContext context) {
		// TODO Auto-generated method stub
		Object fieldValue = new BeanWrapperImpl(value).getPropertyValue(field);
		Object fieldMatchValue = new BeanWrapperImpl(value).getPropertyValue(fieldMatch);
		if (fieldValue != null && fieldValue.equals(fieldMatchValue)) {
			return true;}else{
				return false;
		}
	}

	
	
}
