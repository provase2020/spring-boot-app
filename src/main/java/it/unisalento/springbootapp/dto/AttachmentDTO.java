package it.unisalento.springbootapp.dto;

import org.springframework.web.multipart.MultipartFile;

public class AttachmentDTO {
	
	int id;
	MultipartFile file;
	String lat;
	String lon;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public MultipartFile getFile() {
		return file;
	}
	public void setFile(MultipartFile file) {
		this.file = file;
	}
	public String getLat() {
		return lat;
	}
	public void setLat(String lat) {
		this.lat = lat;
	}
	public String getLon() {
		return lon;
	}
	public void setLon(String lon) {
		this.lon = lon;
	}
	
	
	
}
