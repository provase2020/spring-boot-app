package it.unisalento.springbootapp.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import it.unisalento.springbootapp.validators.FieldsValueMatchConstraint;
import it.unisalento.springbootapp.validators.NamePersonConstraint;

@FieldsValueMatchConstraint.List({
	@FieldsValueMatchConstraint(
			field = "password",
			fieldMatch = "passwordVerify" )
})

public class UserDTO {
	
	int id;
	@NotBlank
	@NamePersonConstraint(name="Osvaldo")
	String name;
	@NotBlank
	String surname;
	String type;
	@Email
	@NotBlank
	String email;
	String password;
	String passwordVerify;
	
	public String getPasswordVerify() {
		return passwordVerify;
	}
	public void setPasswordVerify(String passwordVerify) {
		this.passwordVerify = passwordVerify;
	}

	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
}
