package it.unisalento.springbootapp.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import it.unisalento.springbootapp.entities.Warning;


@Repository
public interface WarningRepository extends JpaRepository<Warning, Integer> {
	
	public List<Warning> findByType (String Type);
	
	@Query("select w from Warning w where w.interventionNote.id=:id")
	public List<Warning> findByIntNotId(@Param("id")int interventionNoteId);
	

}
