package it.unisalento.springbootapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.unisalento.springbootapp.entities.InterventionNote;


@Repository
public interface InterventionNoteRepository extends JpaRepository<InterventionNote, Integer> {

}
