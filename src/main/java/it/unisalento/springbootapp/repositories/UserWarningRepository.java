package it.unisalento.springbootapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.unisalento.springbootapp.entities.UserWarning;

@Repository
public interface UserWarningRepository extends JpaRepository<UserWarning,Integer> {

}
