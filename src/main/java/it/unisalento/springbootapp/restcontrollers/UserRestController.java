package it.unisalento.springbootapp.restcontrollers;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import it.unisalento.springbootapp.dto.UserDTO;
import it.unisalento.springbootapp.entities.User;
import it.unisalento.springbootapp.exceptions.UserNotFoundException;
import it.unisalento.springbootapp.iservices.IUserService;
import it.unisalento.springbootapp.repositories.UserRepository;
import it.unisalento.springbootapp.servicesimpl.UserServiceimpl;

@RestController
@RequestMapping("/api/user")
public class UserRestController {
	
	@Autowired
	IUserService userService;
	
	@RequestMapping(value="/getAll", method=RequestMethod.GET)
	public List<UserDTO> getAll(){
		
		List<UserDTO> list = new ArrayList<UserDTO>();
		UserDTO userDTO1 = new UserDTO();
		userDTO1.setName("Osvaldo");
		userDTO1.setSurname("Rossi");
		userDTO1.setType("security_agent");
		
		UserDTO userDTO2 = new UserDTO();
		userDTO2.setName("Franco");
		userDTO2.setSurname("Verdi");
		userDTO2.setType("security_manager");
		
		list.add(userDTO1);
		list.add(userDTO2);
		
		return list;
	}
	
	@RequestMapping(value="/getById/{id}", method = RequestMethod.GET)
	public UserDTO getById (@PathVariable int id) throws UserNotFoundException {
		System.out.println("id:" +id);
		
		if(id == 4) {
			throw new UserNotFoundException();
		}
		
		UserDTO userDTO1 = new UserDTO();
		userDTO1.setName("Osvaldo");
		userDTO1.setSurname("Rossi");
		userDTO1.setType("security_agent");
		userDTO1.setId(id);
		
		return userDTO1;
	}
	
	/*
	@RequestMapping(value="/save" , 
			method=RequestMethod.POST,
			consumes = MediaType.APPLICATION_JSON_VALUE)	
	*/
	@PostMapping(value="/save", consumes = MediaType.APPLICATION_JSON_VALUE)
	public UserDTO save(@RequestBody @Valid UserDTO userDTO) {
		
		User user = new User();
		
	    user.setEmail(userDTO.getEmail());
	    user.setName(userDTO.getName());
	    user.setSurname(userDTO.getSurname());
	    user.setPassword(userDTO.getPassword());
	    user.setType(userDTO.getType());
	    
	    User userSaved = userService.save(user);
		
		System.out.println("user name:"+ userDTO.getName());
		System.out.println("user surname:"+ userDTO.getSurname());
		userDTO.setId(userSaved.getId());
		
		return userDTO;
		
	}
	
	@GetMapping(value="/getByNameAndType/{name}/{type}")
	public List<UserDTO> getByNameAndType(@PathVariable("name") String name,
			                              @PathVariable("type") String type
			){
		
		List<UserDTO> list = new ArrayList<UserDTO>();
		UserDTO userDTO1 = new UserDTO();
		userDTO1.setName("Osvaldo");
		userDTO1.setSurname("Rossi");
		userDTO1.setType("security_agent");
		
		UserDTO userDTO2 = new UserDTO();
		userDTO2.setName("Franco");
		userDTO2.setSurname("Verdi");
		userDTO2.setType("security_manager");
		
		list.add(userDTO1);
		list.add(userDTO2);
		
		return list;
		
		}

}
